import axios from 'axios';
import { template } from 'underscore';

class EmployeesTable {
    constructor(el) {
        this.table = el;
        this.tableBody = this.table.querySelector('.js-employees-table-body');
        this.tableBodyRows = null;

        this.dayInMilliseconds = 24 * 60 * 60 * 1000;
        this.minuteInMilliseconds = 60 * 1000;
        this.hourInMilliseconds = 60 * 60 * 1000;

        this.classes = {
            active: 'is-active',
            hidden: 'is-hidden',
        };

        this.init();
    }

    init() {
        const PreloaderTemplate = document.getElementById('preloader-template');
        const preloaderTmpl = template(PreloaderTemplate.innerHTML);
        this.tableBody.innerHTML = preloaderTmpl({});

        axios({
            method: this.table.getAttribute('data-method') || 'get',
            url: this.table.getAttribute('data-url'),
        })
            .then((response) => {
                const receivedData = response.data;
                switch (receivedData.status) {
                    case 'GET_EMPLOYEES_LIST_SUCCESS':
                        setTimeout(() => {
                            this.tableBody.innerHTML = this.getEmployeesData(receivedData.data);

                            this.setListeners();
                        }, 2000);
                        
                        break;
                    case 'GET_EMPLOYEES_LIST_FAIL':
                        this.tableBody.innerHTML = '';
                        console.error(receivedData.data.errorMessage);
                        console.error(e);
                        break;
                    default:
                        console.error(e);
                        break;
                }
            })
            .catch(() => {
                console.error(e);
            });
    }

    /**
     * Формируем объекты для каждого сотрудника и создаём содержимое таблицы через template
     * @param data - данные из AJAX-запроса 
     */
    getEmployeesData(data) {
        const TableRowTemplate = document.getElementById('table-body-row-template');
        const tmpl = template(TableRowTemplate.innerHTML);
        let employees = '';

        data.actual.forEach((item, idx) => {
            const virtualDateFrom = new Date(data.virtual[idx].dateFrom);
            const virtualDateTo = new Date(data.virtual[idx].dateTo);
            const actualDateFrom = new Date(item.dateFrom);
            const actualDateTo = new Date(item.dateTo);

            if (virtualDateFrom > virtualDateTo || actualDateFrom > actualDateTo) return '';
            if (virtualDateTo - virtualDateFrom > this.dayInMilliseconds || actualDateTo - actualDateFrom > this.dayInMilliseconds) return '';

            // virtual time
            const virtualTimeBlockWidth = this.getTimeBlockWidth(virtualDateFrom, virtualDateTo);

            if (Number.isNaN(virtualTimeBlockWidth)) return '';
            const [virtualTimeBlockPosition, virtualWidth, reserveVirtualWidth] = this.getTimeBlockPositionAndWidth(virtualDateFrom, virtualTimeBlockWidth);


            // actual time
            const actualTimeBlockWidth = this.getTimeBlockWidth(actualDateFrom, actualDateTo);

            if (Number.isNaN(actualTimeBlockWidth)) return '';
            const [actualTtimeBlockPosition, actualWidth, reserveActualWidth] = this.getTimeBlockPositionAndWidth(actualDateFrom, actualTimeBlockWidth);

            const tmplData = {
                id: item.id,
                name: item.name,
                workPlace: item.workPlace,
                role: item.role,
                virtual: {
                    virtualTimeBlockPosition,
                    virtualWidth,
                    reserveVirtualWidth,
                },
                virtualDate: {
                    days: this.getAvailableDays(virtualDateFrom, virtualDateTo),
                    months: this.getAvailableMonths(virtualDateFrom, virtualDateTo),
                    years: this.getAvailableYears(virtualDateFrom, virtualDateTo),
                },
                actual: {
                    actualWidth,
                    actualTtimeBlockPosition,
                    reserveActualWidth,
                },
                actualDate: {
                    days: this.getAvailableDays(actualDateFrom, actualDateTo),
                    months: this.getAvailableMonths(actualDateFrom, actualDateTo),
                    years: this.getAvailableYears(actualDateFrom, actualDateTo),
                }
            };

            employees += tmpl(tmplData);
        });

        return employees;
    }

    /**
     * Считаем ширину временного блока в %
     * @param from - дата начала
     * @param to - дата завершения
     */
    getTimeBlockWidth(from, to) {
        return +(Math.abs(from.getTime() - to.getTime()) / this.dayInMilliseconds * 100).toFixed(5);
    }

    /**
     * Получаем точку отсчёта и временные блоки (virtual, virtual-reserve || actual, actual-reserve)
     * @param from - дата начала
     * @param width - ширина временного блока в %
     */
    getTimeBlockPositionAndWidth(from, width) {
        const hours = from.getHours();
        const minutes = from.getMinutes();

        const position = +(((hours * this.hourInMilliseconds) + (minutes * this.minuteInMilliseconds)) / this.dayInMilliseconds * 100).toFixed(5);

        if (position + width > 100) {
            const percentageMainWidth = 100 - position;
            const percentageReserveWidth = position + width - 100;
            
            return [position + '%', percentageMainWidth + '%', percentageReserveWidth + '%'];
        }

        return [position + '%', width + '%',  null]; 
    }

    /**
     * Ищем список дней (virtual || actual)
     * @param from - время начала
     * @param to - время завершения
     */
    getAvailableDays(from, to) {
        return Array.from(new Set([from.getDate(), to.getDate()]));
    }

    /**
     * Ищем список месяцев (virtual || actual)
     * @param from - время начала
     * @param to - время завершения
     */
    getAvailableMonths(from, to) {
        return Array.from(new Set([from.getMonth() + 1, to.getMonth() + 1]));
    }

    /**
     * Ищем список лет (virtual || actual)
     * @param from - время начала
     * @param to - время завершения
     */
    getAvailableYears(from, to) {
        return Array.from(new Set([from.getFullYear(), to.getFullYear()]));
    }

    setListeners() {
        this.tableBody.addEventListener('click', (e) => {
            const clickedRow = e.target.closest('.js-table-body-row');

            if (clickedRow) {
                this.toggleRow(clickedRow);
            }
        });
    }

    /**
     * Переключаем actual и virtual временные блоки
     * @param row - строка из body таблицы
     */
    toggleRow(row) {
        const virtualTimeBlocks = row.querySelectorAll('.js-virtual-time');
        const actualTimeBlocks = row.querySelectorAll('.js-actual-time');

        if (virtualTimeBlocks[0].classList.contains(this.classes.active)) {
            this.toggleTimeBlocks(virtualTimeBlocks, this.classes.active, this.classes.hidden, 250);
            this.toggleTimeBlocks(actualTimeBlocks, this.classes.hidden, this.classes.active, 33);
        } else {
            this.toggleTimeBlocks(virtualTimeBlocks, this.classes.hidden, this.classes.active, 33);
            this.toggleTimeBlocks(actualTimeBlocks, this.classes.active, this.classes.hidden, 250);
        }
    }

    /**
     * Переключение классов для отображения нужного временного блока (actual или virtual)
     * @param timeBlocks - временной блок
     * @param removeClass - класс для удаления
     * @param addClass - класс для добавления
     * @param time - задержка для setTimeout
     */
    toggleTimeBlocks(timeBlocks, removeClass, addClass, time) {
        timeBlocks.forEach(timeBlock => {
            timeBlock.classList.remove(removeClass);
            setTimeout(() => {
                timeBlock.classList.add(addClass);
            }, time);
        });
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const items = document.querySelectorAll('.js-employees-table');
    for (let i = 0; i < items.length; i++) {
        new EmployeesTable(items[i]);
    }
});
