import _debounce from 'lodash/debounce';

class Header {
    constructor(el) {
        this.header = el;
        this.headerTitle = this.header.querySelector('.js-header-title');
        this.main = document.querySelector('.js-main');

        this.classes = {
            scroll: 'scroll',
        };

        this.setListeners();
    }

    setListeners() {
        window.addEventListener('scroll', _debounce(() => {
            if (window.pageYOffset !== 0) {
                this.headerTitle.classList.add(this.classes.scroll);
                this.main.classList.add(this.classes.scroll);
            } else {
                this.headerTitle.classList.remove(this.classes.scroll);
                this.main.classList.remove(this.classes.scroll);
            }  
        }, 30));
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const items = document.querySelectorAll('.js-header');
    for (let i = 0; i < items.length; i++) {
        new Header(items[i]);
    }
});
