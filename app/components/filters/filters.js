class Filters {
    constructor(el) {
        this.filters = el;
        
        this.openBtn = this.filters.querySelector('.js-open-btn');
        this.closeBtn = this.filters.querySelector('.js-close-btn');
        this.form = this.filters.querySelector('.js-filters-form');

        this.submitBtn = this.filters.querySelector('.js-filters-submit');
        this.resetBtn = this.filters.querySelector('.js-filters-reset');
        
        this.selects = this.filters.querySelectorAll('.js-select');
        this.errorBlocks = this.filters.querySelectorAll('.js-error');

        this.classes = {
            active: 'is-active',
            hidden: 'is-hidden',
            filterHide: 'hide-by-filter',
        };

        this.setListeners();
    }

    setListeners() {
        this.openBtn.addEventListener('click', () => {
            this.form.classList.add(this.classes.active);
        });
        
        this.closeBtn.addEventListener('click', () => {
            this.form.classList.remove(this.classes.active);
        });

        this.submitBtn.addEventListener('click', () => {
            const optionValues = [];
            
            this.selects.forEach(select => {
                optionValues.push(select.value);
            });

            if (this.validationCheck(optionValues)) {
                this.form.classList.remove(this.classes.active);
                this.filterEmployees(optionValues);
            }
        });

        this.resetBtn.addEventListener('click', () => {
            const optionValues = [];

            this.selects.forEach(select => {
                const option = select.querySelector('.js-main-option');

                option.selected = true;
                optionValues.push(option.value);
            });

            this.validationCheck(optionValues);

            this.form.classList.remove(this.classes.active);
            this.filterEmployees(optionValues);
        });
    }

    /**
     * Валидация select
     * @param optionValues - значения выбранных option
     */
    validationCheck(optionValues) {
        let count = 0;
        let isCorrect = true;

        for (let i = 0; i < optionValues.length; i += 2) {
            if (+optionValues[i] > +optionValues[i + 1]) {
                this.errorBlocks[count].classList.remove(this.classes.hidden);
                isCorrect = false;
            } else {
                this.errorBlocks[count].classList.add(this.classes.hidden);
            }

            count += 1;
        }

        return isCorrect;
    }

    /**
     * Фильтрация таблицы
     * @param options - значения выбранных option
     */
    filterEmployees(options) {
        const availableYears = this.getAvailableDates(options[0], options[1]);
        const availableMonths = this.getAvailableDates(options[2], options[3]);
        const availableDays = this.getAvailableDates(options[4], options[5]);
        
        const emploeeys = document.querySelectorAll('.js-table-body-row');

        emploeeys.forEach(employee => {
            const days = this.intersectionOfArrays(employee, 'data-days', availableDays);
            const months = this.intersectionOfArrays(employee, 'data-months', availableMonths);
            const years = this.intersectionOfArrays(employee, 'data-years', availableYears);
            
            if (days.length && months.length && years.length) {
                employee.classList.remove(this.classes.hidden);
                const timeBlocks = employee.querySelectorAll('.js-time-block');
                
                timeBlocks.forEach(timeBlock => {
                    const suitableDate = this.isSuitableDate(timeBlock, days, months, years);
                    
                    if (!suitableDate) {
                        timeBlock.classList.add(this.classes.filterHide);
                    } else {
                        timeBlock.classList.remove(this.classes.filterHide);
                    }
                });
            } else {
                employee.classList.add(this.classes.hidden);
            }
        });
    }

    /**
     * Генерация массива с элементами от date1 до date2 с шагом 1
     * @param date1 - начальное число (дата)
     * @param date2 - конечное число (дата)
     */
    getAvailableDates(date1, date2) {
        const dates = [];

        for (var i = date1; i <= date2; i++) {
            dates.push(String(i));
        }

        return dates;
    }

    /**
     * Результат пересечения 2-х массивов 
     * @param domElement - DOM элемент
     * @param attribute - дата-атрибут
     * @param array - проверяемый массив
     */
    intersectionOfArrays(domElement, attribute, array) {
        return domElement.getAttribute(attribute).split(',').filter(item => array.includes(item));
    }

    /**
     * Проверяем, подходит ли временной блок для выбранного диапазона
     * @param timeBlock - временной блок
     * @param days - доступные дни
     * @param months - доступные месяцы
     * @param years - доступные годы
     */
    isSuitableDate(timeBlock, days, months, years) {
        const day = this.intersectionOfArrays(timeBlock, 'data-day', days);
        const month = this.intersectionOfArrays(timeBlock, 'data-month', months);
        const year = this.intersectionOfArrays(timeBlock, 'data-year', years);
        
        if (day.length && month.length && year.length) {
            return true;
        }

        return false;
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const items = document.querySelectorAll('.js-filters');
    for (let i = 0; i < items.length; i++) {
        new Filters(items[i]);
    }
});
